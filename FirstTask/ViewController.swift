//
//  ViewController.swift
//  FirstTask
//
//  Created by Julia on 7/5/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit
/*
protocol PassMessageDelegate {
    func passMessage(message:String)
}
*/


class ViewController: UIViewController {

    var message = ""
   // var delegate: PassMessageDelegate?
    
    @IBOutlet weak var FirstViewTF: UITextField!
    
    let fromSecoundName = Notification.Name(rawValue: fromSecoundNotification)

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        FirstViewTF.text = message
        createObservers()
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.changeMessage(notification:)), name: fromSecoundName, object: nil)
        
    }
    
    @objc func changeMessage(notification: NSNotification) {
        FirstViewTF.text = notification.userInfo?[messageKey] as! String

    }

    @IBAction func OpenSecoundViewControllerBtn(_ sender: Any) {
        let textFieldContent = FirstViewTF.text
        let secoundVC = storyboard?.instantiateViewController(identifier: "SecoundViewController") as! SecoundViewController
        secoundVC.createObservers()
        
        let messageToSend:[String: String] = [messageKey: textFieldContent!]

        let fromMainName = Notification.Name(rawValue: fromMainNotification)
        NotificationCenter.default.post(name: fromMainName, object: nil, userInfo: messageToSend)
      /*
        delegate = secoundVC
        secoundVC.delegate = self
        delegate?.passMessage(message: textFieldContent!)
      */
        present(secoundVC, animated: true, completion: nil)
    }
    
}
/*
extension ViewController: PassMessageAgainDelegate{
    func passMessageAgain(message: String) {
        self.dismiss(animated: true) {
            self.message = message
            self.FirstViewTF.text = message
        }
    }
}
*/

//
//  SecoundViewController.swift
//  FirstTask
//
//  Created by Julia on 7/5/20.
//  Copyright © 2020 Bassam. All rights reserved.
//

import UIKit
/*
protocol PassMessageAgainDelegate {
    func passMessageAgain(message:String)
}
*/

class SecoundViewController: UIViewController {

    var message = ""
    //var delegate: PassMessageAgainDelegate?
    @IBOutlet weak var SecoundViewTF: UITextField!
    
    let fromMainName = Notification.Name(rawValue: fromMainNotification)

    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        SecoundViewTF.text = message
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(SecoundViewController.changeMessage(notification:)), name: fromMainName, object: nil)
        
    }
    
    @objc func changeMessage(notification: NSNotification) {
        message = notification.userInfo?[messageKey] as! String
    }
    
    @IBAction func OpenFirstScreenBtn(_ sender: Any) {
        let textFieldContent = SecoundViewTF.text
        
        let messageToSend:[String: String] = [messageKey: textFieldContent!]
        
        let fromSecoundName = Notification.Name(rawValue: fromSecoundNotification)
        NotificationCenter.default.post(name: fromSecoundName, object: nil, userInfo: messageToSend)

        dismiss(animated: true, completion: nil)
    }
    
}
/*
extension SecoundViewController: PassMessageDelegate {
    func passMessage(message: String) {
        self.dismiss(animated: true) {
            self.message = message
        }
    }
}
*/
